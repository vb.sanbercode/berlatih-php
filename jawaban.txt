1. CREATE DATABASE myshop;
2. 
CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NULL DEFAULT NULL,
	email VARCHAR(255) NULL DEFAULT NULL,
	password VARCHAR(255) NULL DEFAULT NULL,
	PRIMARY KEY (`id`) 
);

CREATE TABLE items (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	description VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	price INT NULL DEFAULT NULL,
	stock INT NULL DEFAULT NULL,
	category_id INT NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `FK_items_categories_id` FOREIGN KEY (`category_id`) REFERENCES `mysql`.`categories` (`id`) ON UPDATE CASCADE ON DELETE NO ACTION
);

CREATE TABLE categories (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
);

3. 
INSERT  INTO users (name,email,password) VALUES 
('John Doe','john@doe.com','john123');
INSERT  INTO users (name,email,password) VALUES 
('Jane Doe','jane@doe.com','jenita123');

INSERT INTO categories (name) VALUES ('gadget'); 
INSERT INTO categories (name) VALUES ('cloth'); 
INSERT INTO categories (name) VALUES ('men'); 
INSERT INTO categories (name) VALUES ('women'); 
INSERT INTO categories (name) VALUES ('branded'); 

INSERT INTO items (name, description, price, stock, category_id) VALUES ('Sumsang b50', 'hape keren dari merek sumsang', '4000000', '100', '1'); 
INSERT INTO items (name, description, price, stock, category_id) VALUES ('Uniklooh', 'baju keren dari brand ternama', '500000', '50', '2'); 
INSERT INTO items (name, description, price, stock, category_id) VALUES ('IMHO Watch', ' jam tangan anak yang jujur banget', '2000000', '10', '1'); 

4 a. 
SELECT users.id, users.name, users.email
FROM users;

4 b.
SELECT *
FROM items
WHERE items.price > 1000000;

SELECT *
FROM items
WHERE items.name LIKE '%sang%';

4 c.
SELECT items.name, 
       items.description, 
       items.price, 
       items.stock, 
       items.category_id, 
       categories.name
  FROM items
  JOIN categories ON items.category_id = categories.id;

5.
UPDATE price.items 
   SET price = '2500000'
 WHERE price.id = '1'; 


