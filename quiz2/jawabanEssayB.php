<?php

/**
 * CONTOH:
 * perolehan_medali(
 *  array(
 *    array('Indonesia', 'emas'),
 *    array('India', 'perak'),
 *    array('Korea Selatan', 'emas' ),
 *    array('India', 'perak'),
 *    array('India', 'emas'),
 *    array('Indonesia', 'perak'),
 *    array('Indonesia', 'emas'),
 *  )
 * );
 *
 * output:
 * Array(
 *    Array (
 *      [negara] => 'Indonesia'
 *      [emas] => 2
 *      [perak] => 1
 *      [perunggu] => 0
 *    ),
 *    Array (
 *      [negara] => 'India'
 *      [emas] => 1
 *      [perak] => 2
 *      [perunggu] => 0
 *    ),
 *    Array (
 *      [negara] => 'Korea Selatan'
 *      [emas] => 1
 *      [perak] => 0
 *      [perunggu] => 0
 *    )
 * )
 *
 *
 */

function perolehan_medali($arr){
  // Kode kamu di sini

  $indo = array("negara"=>"","emas"=>0,"perak"=>0,"perunggu"=>0);
  $india = array("negara"=>"","emas"=>0,"perak"=>0,"perunggu"=>0);
  $korsel = array("negara"=>"","emas"=>0,"perak"=>0,"perunggu"=>0);
  

  for($i = 0; $i < count($arr); $i++){

    switch ($arr[$i][0]){
      case "Indonesia":
        
        $indo["negara"] = "Indonesia"; 

        switch ($arr[$i][1]){
          case "emas":
            $indo["emas"] = $indo["emas"] + 1; 
          break;
          case "perak":
            $indo["perak"] = $indo["perak"] + 1; 
          break;
          case "perunggu":
            $indo["perunggu"] = $indo["perunggu"] + 1; 
    
        }
      break;
      case "India":
        
        $india["negara"] = "India"; 

        switch ($arr[$i][1]){
          case "emas":
            $india["emas"] = $india["emas"] + 1; 
          break;
          case "perak":
            $india["perak"] = $india["perak"] + 1; 
          break;
          case "perunggu":
            $india["perunggu"] = $india["perunggu"] + 1; 
    
        }
      break;
      case "Korea Selatan":
        
        $korsel["negara"] = "Korea Selatan"; 

        switch ($arr[$i][1]){
          case "emas":
            $korsel["emas"] = $korsel["emas"] + 1; 
          break;
          case "perak":
            $korsel["perak"] = $korsel["perak"] + 1; 
          break;
          case "perunggu":
            $korsel["perunggu"] = $korsel["perunggu"] + 1; 
    
        }
      break;
    }

  }

  return array($indo,$india,$korsel);

}
 
// echo perolehan_medali(array());
 
// TEST CASES
 print_r (
   perolehan_medali(
   array(
    array('Indonesia', 'emas'),
    array('India', 'perak'),
    array('Korea Selatan', 'emas' ),
    array('India', 'perak'),
    array('India', 'emas'),
    array('Indonesia', 'perak'),
    array('Indonesia', 'emas')
   )
  ));
/**
 * output:
 * Array(
 *    Array (
 *      [negara] => 'Indonesia'
 *      [emas] => 2
 *      [perak] => 1
 *      [perunggu] => 0
 *    ),
 *    Array (
 *      [negara] => 'India'
 *      [emas] => 1
 *      [perak] => 2
 *      [perunggu] => 0
 *    ),
 *    Array (
 *      [negara] => 'Korea Selatan'
 *      [emas] => 1
 *      [perak] => 0
 *      [perunggu] => 0
 *    )
 * )
 */
 
// print_r(perolehan_medali([])); // no data
?>