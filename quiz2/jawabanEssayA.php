<?php
  function hitung($string_data)
  {
    $a = 0;
    $b = 0;

    if (strpos($string_data,"+") > 0){
      $a = substr($string_data,0,strpos($string_data,"+"));
      $b = substr($string_data,strpos($string_data,"+") + 1,strlen($string_data) - strpos($string_data,"+"));
      return $a + $b;
    }elseif (strpos($string_data,"-") > 0){
      $a = substr($string_data,0,strpos($string_data,"-"));
      $b = substr($string_data,strpos($string_data,"-") + 1,strlen($string_data) - strpos($string_data,"-"));
      return $a-$b;
    }elseif (strpos($string_data,"*") > 0){
      $a = substr($string_data,0,strpos($string_data,"*"));
      $b = substr($string_data,strpos($string_data,"*") + 1,strlen($string_data) - strpos($string_data,"*"));
      return $a*$b;
    }elseif (strpos($string_data,":") > 0){
      $a = substr($string_data,0,strpos($string_data,":"));
      $b = substr($string_data,strpos($string_data,":") + 1,strlen($string_data) - strpos($string_data,":"));
      return $a/$b;
    }elseif (strpos($string_data,"%") > 0){
      $a = substr($string_data,0,strpos($string_data,"%"));
      $b = substr($string_data,strpos($string_data,"%") + 1,strlen($string_data) - strpos($string_data,"%"));
      return $a%$b;
    }
  }

  //TEST CASES
  echo hitung("102*2"); //204
  echo("<br><br>");
  echo hitung("2+3"); //5
  echo("<br><br>");
  echo hitung("100:25"); //4
  echo("<br><br>");
  echo hitung("10%2"); //0
  echo("<br><br>");
  echo hitung("99-2"); //97
  echo("<br><br>");

?>