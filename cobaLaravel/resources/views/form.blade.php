<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
  <form action="{{ url('/kirim') }}" method="POST">
  @csrf
		<h2>Buat Account Baru!</h2>

		<h4>Sign Up Form</h4>

		<label for="first_name">First name:</label>
		<br>
		
		<input type="text" name="first_name" id="first_name">
		<br>
		<br>

		<label for="last_name">Last name:</label>
		<br>

		<input type="text" name="last_name" id="last_name">
		<br>
		<br>

		<label>Gender:</label>
		<br>
		<br>

		<input type="radio" name="gender" value="Male" checked>Male
		<br>
		<input type="radio" name="gender" value="female">Female
		<br>
		<input type="radio" name="gender" value="other">Other 
		<br>
		<br>

		<label>Nationality:</label>
		<br>
		<br>

		<select name="nasional" id="nasional">
      <option value="indonesia">Indonesia</option>
      <option value="singapura">Singapura</option>
      <option value="malaysia">Malaysia</option>
      <option value="australia">Australia</option>
		</select>
		<br>
		<br>

		<label>Language Spoken:</label>
		<br>
		<br>

		<input type="checkbox" name="bahasa" checked>Bahasa Indonesia
		<br>
		<input type="checkbox" name="English">English
		<br>
		<input type="checkbox" name="Other">Other
		<br>
		<br>

		<label>Bio:</label>
		<br>
		<br>

		<textarea cols="30" rows="10"></textarea>
		<br>

		<input type="submit" value="Sign Up">

	</form>
	
</body>
</html>