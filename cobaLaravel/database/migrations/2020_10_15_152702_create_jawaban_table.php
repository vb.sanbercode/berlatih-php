<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJawabanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jawaban', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->longText('isi')->nullable();
          $table->date('tanggal_dibuat', 0)->nullable();
          $table->date('tanggal_diperbaharui', 0)->nullable();
          $table->unsignedBigInteger('pertanyaan_id');
          $table->unsignedBigInteger('profil_id');
          $table->timestamps();
          $table->foreign('profil_id')->references('id')->on('profil');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jawaban');

    }
}
