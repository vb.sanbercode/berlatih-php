<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    //
    public function index(){
      $posts = DB::table('pertanyaan')->get();

      // dd($posts);

      return view('crud.index',compact('posts'));
    }

    public function create(){
      return view('crud.posts.create');
    }

    public function store(Request $request){
      $judul = $request -> input('judul');
      $isi_pertanyaan = $request -> input('isi_pertanyaan');

      $query = DB::table('pertanyaan')->insert([
        'judul' => $judul, 
        'isi' => $isi_pertanyaan,
        'profil_id' => 1]);

      return redirect('/pertanyaan/create');
    }

    public function show(){
      return view('create');
    }

    public function edit(){
      return view('create');
    }

}
