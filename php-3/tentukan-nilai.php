<?php
function tentukan_nilai($number)
{
    //  kode disini
    if (($number <= 100) && ($number >= 85)) {
      return "Sangat Baik <br><br>";
    }elseif (($number < 85) && ($number >= 70)) {
      return "Baik <br><br>";
    }elseif (($number < 70) && ($number >= 60)) {
      return "Cukup <br><br>";
    }else {
      return "Kurang <br><br>";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>