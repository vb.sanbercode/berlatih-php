<?php
  require_once 'animal.php';

  class Frog extends Animal {

    function __construct($name_frog) {

      parent::__construct($name_frog,4);

    }

    function jump(){
      return "hop hop";
    }

  }

?>