<?php
  require_once 'animal.php';

  class Ape extends Animal {

    function __construct($name_ape) {

      parent::__construct($name_ape);

    }

    function yell(){
      return "Auooo";
    }

  }

?>