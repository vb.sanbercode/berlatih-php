<?php
  require 'Frog.php';
  require 'Ape.php';

  $sheep = new Animal("shaun");

  echo $sheep->get_name(); // "shaun"
  echo "<br><br>";
  echo $sheep->legs; // 2
  echo "<br><br>";
  echo $sheep->get_cold_blooded(); // false
  echo "<br><br>";

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

$sungokong = new Ape("kera sakti");

echo $sungokong->get_name(); // "shaun"
echo "<br><br>";
echo $sungokong->legs; // 2
echo "<br><br>";
echo $sungokong->get_cold_blooded(); // false
echo "<br><br>";

echo($sungokong->yell()); // "Auooo"

echo "<br><br>";

$kodok = new Frog("buduk");

echo $kodok->get_name(); // "shaun"
echo "<br><br>";
echo $kodok->legs; // 2
echo "<br><br>";
echo $kodok->get_cold_blooded(); // false
echo "<br><br>";

echo($kodok->jump()) ; // "hop hop"

echo "<br><br>";
  

?>